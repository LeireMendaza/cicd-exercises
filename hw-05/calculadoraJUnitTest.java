import calculadora.calculadora;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author menda
 */
public class calculadoraJUnitTest {
    @Test
    public void testSuma() {
        int resultado = calculadora.suma(2, 3);
        int esperado = 5;
        assertEquals(esperado, resultado);
    }
    @Test
    public void testSuma2() {
        int resultado = calculadora.suma(1, 2);
        int esperado = 3;
        assertEquals(esperado, resultado);
    }
}
