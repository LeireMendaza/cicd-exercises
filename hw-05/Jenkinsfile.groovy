pipeline {
    agent any
    stages {
        stage('Build project') {
            steps {
                script {
                    echo 'javac calculadora.java'
                }
            }
        }
        stage('Run project') {
            steps {
                script {
                    echo 'java calculadora'
                }
            }
        }
        stage('Test project') {
            steps {
                script {
                    echo 'javac calculadoraJUnitTest.java'
                    echo 'java calculadoraJUnitTest'
                }
            }
        }
        stage('Publish project') {
            steps {
                script {
                    echo 'publishing project'
                }
            }
        }
        stage('Deploy project') {
            steps {
                script {
                    echo 'Deploy project'
                }
            }
        }
    }
}